<div>
    <div class="flex flex-wrap justify-between">
        <div class="w-1/4 lg:w-1/2 p-1">
            <div class="bg-red-100 border border-red-200 rounded shadow p-1 w-full">
                <small class="text-red-900">Testovaných</small>
                <div class="text-right">
                    <span class="text-red-800 text-2xl font-bold">{{ $infectedStats['tests'] }}</span>
                    <span class="text-sm text-red-400">({{ sprintf('%2.2f%%', 100 * $infectedStats['tests'] / 10649800 )}})</span>

                </div>
            </div>
        </div>
        <div class="w-1/4 lg:w-1/2 p-1">
            <div class="bg-teal-100 border border-teal-200 rounded shadow p-1 w-full">
                <small class="text-teal-800">Uzdravených</small><br/>
                <div class="text-right text-teal-800">
                    <span class="text-2xl font-bold">{{ $infectedStats['recovered'] }}</span>
                    <span class="text-sm text-teal-400">({{ sprintf('%2.2f%%', 100 * $infectedStats['recovered'] / $infectedStats['infected'])}})</span>
                </div>
            </div>
        </div>
        <div class="w-1/4 lg:w-1/2 p-1">
            <div class="bg-orange-100 border border-orange-200 rounded shadow p-1 w-full">
                <small class="text-orange-800">Nakažených</small><br/>
                <div class="text-orange-800 text-right">
                    <span class="text-2xl font-bold">{{ $infectedStats['infected'] }}</span>
                    <span class="text-sm text-orange-400">({{ sprintf('%2.2f%%', 100 * $infectedStats['infected'] / $infectedStats['tests'])}})</span>
                </div>
            </div>
        </div>
        <div class="w-1/4 lg:w-1/2 p-1">
            <div class="bg-gray-100 border border-gray-200 rounded shadow p-1 w-full">
                <small>Mrtvých</small><br/>
                <div class="text-gray-800 text-right">
                    <span class="text-2xl font-bold">{{ $infectedStats['dead'] }}</span>
                    <span class="text-sm text-gray-500">({{ sprintf('%2.2f%%', 100 * $infectedStats['dead'] / $infectedStats['infected'])}})</span>
                </div>
            </div>
        </div>
    </div>
    <div class="mx-2 text-right text-sm text-gray-600">
        Zdroj: <a href="https://onemocneni-aktualne.mzcr.cz/covid-19">MZ ČR</a>
    </div>
</div>
