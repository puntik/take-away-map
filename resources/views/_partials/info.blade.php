<div class="p-2 text-sm text-gray-800 tracking-tight leading-tight ">
    <div class="h-6 mb-2">
        <img src="/images/icon/green.png" class="h-6 float-left mr-2">
        Prodej jídla v restauraci přes okénka
    </div>
    <div class="h-6">
        <img src="/images/icon/red.png" class="h-6 float-left mr-2">
        Galanterie - prodej potřeb pro šití roušek
    </div>
</div>
