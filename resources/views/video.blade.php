@extends('layouts.www')

@section('main')
    <div class="w-full px-4">
        <div class="flex">
            <div class="bg-red-100 w-full">
                <video class="w-full border-0" height="480" controls>
                    <source src="{{ \Illuminate\Support\Facades\Storage::url('ZOOM0001.mp4') }}" type="video/mov">
                    Your browser does not support the video tag.
                </video>
            </div>
            <div class="bg-red-200 w-1/4 max-w-2xl">
                Up next
            </div>
        </div>
        {{ \Illuminate\Support\Facades\Storage::url('ZOOM0001.mp4') }}
        Sem dame komentare ..
    </div>
@endsection
