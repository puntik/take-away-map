@extends('layouts.www')

@section('main')
    <div class="lg:flex">
        <div class="w-full lg:w-1/3">
            @include('_partials.covid-statistics')
        </div>

        <div class="w-full lg:w-2/3">
            <covid-graph-component
                tests="{{ $infectedStats['tests'] - ($infectedStats['recovered'] +  $infectedStats['dead'] + $infectedStats['infected'])}}"
                recovered="{{ $infectedStats['recovered'] }}"
                dead="{{ $infectedStats['dead'] }}"
                infected="{{ $infectedStats['infected'] - ($infectedStats['recovered'] + $infectedStats['dead'])}}"
            ></covid-graph-component>
        </div>
    </div>
@endsection
