@extends('layouts.www')

@section('main')
    <div class="flex min-h-full">
        <div class="w-1/4 mr-1">
            <div class="bg-gray-100 shadow p-2">
                <helplines-component></helplines-component>
            </div>
            <div class="bg-gray-100 shadow mt-2">
                <example-component></example-component>
            </div>
            <div class="bg-gray-100 shadow mt-2">
                @include('_partials.info')
            </div>
        </div>

        <div class="h-full w-3/4">
            <map-component></map-component>
        </div>
    </div>
@endsection
