<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBulkContainersTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bulk_containers', function (Blueprint $table) {
            $table->id();

            $table->string('title', 256);
            $table->double('lat');
            $table->double('lng');
            $table->timestamp('available_from')->nullable();
            $table->timestamp('available_to')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bulk_containers');
    }
}
