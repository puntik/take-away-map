<?php

namespace Deployer;

require 'recipe/laravel.php';

// Project name
set('application', 'take-away-map');

// Project repository
set('repository', 'git@bitbucket.org:puntik/take-away-map.git');

// [Optional] Allocate tty for git clone. Default value is false.
set('git_tty', true);

// Shared files/dirs between deploys
add('shared_files', [
    'database/database.sqlite',
]);

add('shared_dirs', []);

// Writable dirs by web server
add('writable_dirs', []);

// Hosts

host('o')
    ->set('deploy_path', '~/www/abetzi.cz/{{application}}');

// Tasks
desc('Build application');
task('build', function () {
    run('cd {{release_path}} && build');
});

desc('Reload php fast process manager workers');
task('fpm:reload', function () {
    run('sudo /etc/init.d/php-fpm reload');
});

// [Optional] if deploy fails automatically unlock.
after('deploy:failed', 'deploy:unlock');

// Migrate database before symlink new release.

before('deploy:symlink', 'artisan:migrate');
after('deploy:symlink', 'fpm:reload');
