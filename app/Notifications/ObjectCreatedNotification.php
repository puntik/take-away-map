<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\SlackAttachment;
use Illuminate\Notifications\Messages\SlackMessage;
use Illuminate\Notifications\Notification;

class ObjectCreatedNotification extends Notification implements ShouldQueue
{

    use Queueable;

    /**
     * @var \App\Source
     */
    private $source;

    public function __construct(\App\Source $source)
    {
        $this->source = $source;
    }

    public function via($notifiable)
    {
        return ['slack'];
    }

    public function toSlack()
    {
        return (new SlackMessage())
            ->success()
            ->from(config('app.name'), ':dog:')
            ->content(sprintf('New object #%d was created. Please check it.', $this->source->id))
            ->attachment(function (SlackAttachment $attachment) {
                $attachment->title('More info about a new object')
                           ->fields([
                               'name'        => $this->source->title,
                               'created'     => $this->source->created_at->format('Y-m-d H:m'),
                               'environment' => config('app.env', 'dev'),
                           ])
                           ->action(
                               'Edit',
                               config('nova.url') . config('nova.path') . '/resources/sources/' . $this->source->id,
                               'primary'
                           );
            });
    }

    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
