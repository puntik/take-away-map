<?php

namespace App\Console\Commands;

use App\Source;
use Illuminate\Console\Command;
use Illuminate\Support\Carbon;

class LoadSourcesFromCsvCommand extends Command
{

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ta:load-csv
        {filename : Source csv file}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Load sources from csv file';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $filename = $this->argument('filename');

        if (! is_file($filename)) {
            $this->error('Source file not found.');

            return;
        }

        $handler = fopen($filename, 'rb');

        while ($data = fgetcsv($handler)) {
            Source::create([
                'type'        => $data[0],
                'title'       => trim($data[1]),
                'lat'         => $data[2],
                'lng'         => $data[3],
                'approved_at' => Carbon::now(),
            ]);
        }

        fclose($handler);
    }
}
