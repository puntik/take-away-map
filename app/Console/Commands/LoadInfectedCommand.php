<?php

namespace App\Console\Commands;

use App\Services\InfectedLoader;
use Illuminate\Console\Command;

class LoadInfectedCommand extends Command
{

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ta:load-infected';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * @var InfectedLoader
     */
    private $infectedLoader;

    public function __construct(InfectedLoader $infectedLoader)
    {
        $this->infectedLoader = $infectedLoader;

        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // Preload data to cache
        $this->infectedLoader->getInfectedData();

        $this->info('New data loaded and saved to cache.');
    }
}
