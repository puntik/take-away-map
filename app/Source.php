<?php

namespace App;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class Source extends Model
{

    protected $table = 'sources';

    protected $fillable = [
        'type',
        'title',
        'lat',
        'lng',
        'approved_at',
    ];

    protected $casts = [
        'lat' => 'float',
        'lng' => 'float',
    ];

    protected $dates = [
        'approved_at',
    ];

    public function scopeApproved(Builder $query)
    {
        return $query->whereNotNull('approved_at');
    }
}
