<?php

namespace App\Nova;

use App\Nova\Actions\ApproveSourceAction;
use App\Nova\Filters\ApprovedSourceFilter;
use App\Nova\Metrics\ApprovedSources;
use App\Nova\Metrics\TotalSources;
use Illuminate\Http\Request;
use Laravel\Nova\Fields\Boolean;
use Laravel\Nova\Fields\DateTime;
use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\Number;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Http\Requests\NovaRequest;

class Source extends Resource
{

    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = \App\Source::class;

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'id';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'id',
    ];

    /**
     * Get the fields displayed by the resource.
     *
     * @param Request $request
     *
     * @return array
     */
    public function fields(Request $request)
    {
        return [
            ID::make()->sortable(),
            Text::make('Title')->sortable(),
            Text::make('Type'),

            Boolean::make('Approved', function () {
                return $this->approved_at !== null;
            }),

            DateTime::make('Approved At')->hideFromIndex(),
            Text::make('Lat')->hideFromIndex(),
            Text::make('Lng')->hideFromIndex(),

            Text::make('mapy.cz', function () {
                return sprintf('https://mapy.cz/zakladni?x=%f&y=%f&z=16', $this->lng, $this->lat);
            })->hideFromIndex(),

        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param Request $request
     *
     * @return array
     */
    public function cards(Request $request)
    {
        return [
            new TotalSources(),
            new ApprovedSources(),
        ];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param Request $request
     *
     * @return array
     */
    public function filters(Request $request)
    {
        return [
            ApprovedSourceFilter::make(),
        ];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param Request $request
     *
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param Request $request
     *
     * @return array
     */
    public function actions(Request $request)
    {
        return [
            ApproveSourceAction::make(),

        ];
    }
}
