<?php declare(strict_types = 1);

namespace App;

use Illuminate\Database\Eloquent\Model;

class BulkContainer extends Model
{

    protected $fillable = [
        'lat',
        'lng',
        'available_from',
        'available_to',
        'title',
    ];

    protected $casts = [
        'lat' => 'float',
        'lng' => 'float',
    ];

    protected $dates = [
        'available_from',
        'available_to',
    ];
}
