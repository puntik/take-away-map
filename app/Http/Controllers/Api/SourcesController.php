<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\SourceResource;
use App\Notifications\ObjectCreatedNotification;
use App\Source;
use App\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Symfony\Component\HttpFoundation\Response;

class SourcesController extends Controller
{

    public const ADMIN_USER = 1;

    public function index()
    {
        return SourceResource::collection(
            Source::approved()->get()
        );
    }

    public function store(Request $request)
    {
        $validated = $this->validate($request, [
            'title' => 'required',
            'lat'   => 'required',
            'lng'   => 'required',
            'type'  => 'required|in:takeaway,disinfection,mask,haberdasher',
        ]);

        $source = Source::create([
            'type'  => $validated['type'],
            'title' => $validated['title'],
            'lat'   => $validated['lat'],
            'lng'   => $validated['lng'],
        ]);

        // Notify admin via slack about a new source
        User::find(self::ADMIN_USER)->notify(
            new ObjectCreatedNotification($source)
        );

        // Log a new source
        Log::info(sprintf('New source #%d created.', $source->id));

        return JsonResponse::create($source, Response::HTTP_CREATED);
    }
}
