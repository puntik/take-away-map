<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Services\InfectedLoader;
use Illuminate\Http\Request;

class HomeController extends Controller
{

    /** @var InfectedLoader */
    private $infectedLoader;

    public function __construct(InfectedLoader $infectedLoader)
    {
        $this->infectedLoader = $infectedLoader;
    }

    public function __invoke()
    {
        $infectedStats = $this->infectedLoader->getInfectedData();

        return view('welcome', compact('infectedStats'));
    }
}
