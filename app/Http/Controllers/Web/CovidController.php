<?php declare(strict_types = 1);

namespace App\Http\Controllers\Web;

use App\Services\InfectedLoader;
use Illuminate\Routing\Controller;

class CovidController extends Controller
{

    /** @var InfectedLoader */
    private $infectedLoader;

    public function __construct(InfectedLoader $infectedLoader)
    {
        $this->infectedLoader = $infectedLoader;
    }

    public function __invoke()
    {
        $infectedStats = $this->infectedLoader->getInfectedData();

        return view('covid', compact('infectedStats'));
    }

}
