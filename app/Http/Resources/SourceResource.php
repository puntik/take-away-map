<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class SourceResource extends JsonResource
{

    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'type'  => $this->type,
            'title' => $this->title,
            'geo'   => [$this->lat, $this->lng],
        ];
        // return parent::toArray($request);
    }
}
