<?php

namespace App\Providers;

use App\Services\CachedInfectedLoader;
use App\Services\InfectedLoader;
use App\Services\MzcrInfectedLoader;
use GuzzleHttp\Client;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(InfectedLoader::class, function (): InfectedLoader {
            $baseClient = new MzcrInfectedLoader(new Client());

            return new CachedInfectedLoader($baseClient, config('services.dejpru.cache_ttl'));
        });
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
