<?php declare(strict_types = 1);

namespace App\Services;

use Illuminate\Support\Facades\Cache;

class CachedInfectedLoader implements InfectedLoader
{

    private const CACHE_KEY = 'infected:data';

    /** @var InfectedLoader */
    private $infectedLoader;

    /** @var int */
    private $ttl;

    public function __construct(InfectedLoader $infectedLoader, int $ttl)
    {
        $this->infectedLoader = $infectedLoader;
        $this->ttl            = $ttl;
    }

    public function getInfectedData(): array
    {
        return Cache::remember(self::CACHE_KEY, $this->ttl, function (): array {
            return $this->infectedLoader->getInfectedData();
        });
    }
}
