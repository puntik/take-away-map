<?php declare(strict_types = 1);

namespace App\Services;

use GuzzleHttp\Client;
use Symfony\Component\DomCrawler\Crawler;

class MzcrInfectedLoader implements InfectedLoader
{

    private const SOURCE_URL = 'https://onemocneni-aktualne.mzcr.cz/covid-19';

    /** @var Client */
    private $client;

    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    public function getInfectedData(): array
    {
        try {
            $response = $this->client->get(self::SOURCE_URL);
            $html     = $response->getBody()->getContents();
        } catch (\Throwable $e) {
            dd($e->getMessage());
        }

        //parse current state
        $crawler = new Crawler($html);

        return [
            'tests'     => (int) str_replace(' ', '', $crawler->filter('#count-test')->text()),
            'infected'  => (int) str_replace(' ', '', $crawler->filter('#count-sick')->text()),
            'recovered' => (int) str_replace(' ', '', $crawler->filter('#count-recover')->text()),
            'dead'      => (int) str_replace(' ', '', $crawler->filter('#count-dead')->text()),
        ];
    }
}
