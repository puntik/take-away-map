<?php declare(strict_types = 1);

namespace App\Services;

interface InfectedLoader
{

    public function getInfectedData(): array;
}
